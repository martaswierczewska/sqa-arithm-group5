package de.rwth.swc.sqa.arithmb;

class Modulo implements Operator {

  public int evaluate(int left, int right) {
    return left % right;
  }

}
