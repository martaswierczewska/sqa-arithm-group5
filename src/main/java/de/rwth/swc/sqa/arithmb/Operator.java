package de.rwth.swc.sqa.arithmb;

interface Operator {
  int evaluate(int left, int right);
}
