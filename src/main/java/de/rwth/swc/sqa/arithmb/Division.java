package de.rwth.swc.sqa.arithmb;

class Division implements Operator {

  public int evaluate(int left, int right) {
    return left / right;
  }

}
