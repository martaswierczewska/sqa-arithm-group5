package de.rwth.swc.sqa.arithmb;

class ExpressionTree {
  private Node root;

  ExpressionTree(Node node) {
    this.root = node;
  }

  public Node getRoot() {
    return root;
  }

  public void setRoot(Node root) {
    this.root = root;
  }

  public void addLeft(Node parent, Node child) {
    parent.setLeftNode(child);
  }

  public void addRight(Node parent, Node child) {
    parent.setRightNode(child);
  }

  public int calculate() {
    return evaluate(this.root);
  }

  private int evaluate(Node node) {
    if (node.isLeaf()) {
      return node.getValue();
    } else {
      return node.getOperator().evaluate(evaluate(node.getLeftNode()),
          evaluate(node.getRightNode()));
    }
  }
}
