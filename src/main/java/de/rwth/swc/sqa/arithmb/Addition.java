package de.rwth.swc.sqa.arithmb;

class Addition implements Operator {

  public int evaluate(int left, int right) {
    return left + right;
  }

}
