package de.rwth.swc.sqa.arithmb;

class Node {
  private Node leftNode;
  private Node rightNode;
  private Node parentNode;
  private int value;
  private Operator operator;

  Node() {}

  Node(int val) {
    this.value = val;
  }

  Node(Operator op) {
    this.operator = op;
  }

  public Node getLeftNode() {
    return leftNode;
  }

  public void setLeftNode(Node leftNode) {
    this.leftNode = leftNode;
    this.leftNode.parentNode = this;
  }

  public Node getRightNode() {
    return rightNode;
  }

  public void setRightNode(Node rightNode) {
    this.rightNode = rightNode;
    this.rightNode.parentNode = this;
  }

  public Node getParentNode() {
    return parentNode;
  }

  public void setParentNode(Node parentNode) {
    this.parentNode = parentNode;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public Operator getOperator() {
    return operator;
  }

  public void setOperator(Operator operator) {
    this.operator = operator;
  }

  public boolean isLeaf() {
    return this.leftNode == null && this.rightNode == null;
  }
}
