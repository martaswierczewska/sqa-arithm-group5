package de.rwth.swc.sqa.arithma;

import static de.rwth.swc.sqa.arithma.Token.Type.*;

class Lexer {
    private String input;
    private int i;

    Lexer(String input) {
        this.input = input;
        this.i = 0;
    }

    public Token getNextToken() {
        while (i < input.length()) {
            char c = input.charAt(i);
            if (Character.isWhitespace(c)) {
                i++;
                continue;
            } else if (Character.isDigit(c)) {
                String integer = "";
                do {
                    integer += c;
                    i++;
                    if (i < input.length()) {
                        c = input.charAt(i);
                    }
                } while (i < input.length() && Character.isDigit(c));
                return new Token(integer, INTEGER);
            } else if (c == '(') {
                i++;
                return new Token(null, LPAREN);
            } else if (c == ')') {
                i++;
                return new Token(null, RPAREN);
            } else if (c == '+' || c == '-' || c == '*' || c == '/' || c == '%') {
                i++;
                switch(c) {
                    case '+':
                        return new Token(""+c, PLUS);
                    case '-':
                        return new Token(""+c, MINUS);
                    case '*':
                        return new Token(""+c, MULTIPLY);
                    case '/':
                        return new Token(""+c, DIVIDE);
                    case '%':
                        return new Token(""+c, MODULO);
                }
            } else {
                throw new RuntimeException("Invalid character \"" + c + "\" at position " + (i + 1));
            }
        }
        return new Token("", EOF);
    }
}
