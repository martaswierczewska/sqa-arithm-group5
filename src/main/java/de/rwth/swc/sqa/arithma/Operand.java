package de.rwth.swc.sqa.arithma;

class Operand extends Node {
    private int value;

    Operand(int value) {
        this.value = value;
    }

    public int evaluate() {
        return this.value;
    }
}
