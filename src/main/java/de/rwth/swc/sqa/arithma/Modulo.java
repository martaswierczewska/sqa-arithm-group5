package de.rwth.swc.sqa.arithma;

class Modulo extends Operator {

    Modulo(Node left, Node right) {
        this.left = left;
        this.right = right;
    }

    public int evaluate() {
        return left.evaluate() % right.evaluate();
    }
}
