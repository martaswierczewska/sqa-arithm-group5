package de.rwth.swc.sqa.arithma;

/*
expr: term ((PLUS | MINUS) term)*
term: factor ((MULIPLY | DIVIDE | MODULO) factor)*
factor: INTEGER | LPAREN expr RPAREN
*/

import static de.rwth.swc.sqa.arithma.Token.Type.*;

class Parser {
    private Lexer lexer;
    private Token token;

    private String accept(Token.Type type) {
        if (this.token.getType() == type) {
            String value = this.token.getValue();
            this.token = this.lexer.getNextToken();
            return value;
        } else {
            throw new RuntimeException("unexpected token: '" + this.token.getValue() + "'");
        }
    }

    Parser(Lexer lexer) {
        this.lexer = lexer;
        this.token = this.lexer.getNextToken();
    }

    public Node parse() {
        Node node = expr();
        if (this.token.getType() != Token.Type.EOF) {
            throw new RuntimeException("Not completely parsed!");
        }
        return node;
    }

    private Node factor() {
        if (token.getType() == INTEGER) {
            return new Operand(Integer.parseInt(accept(INTEGER)));
        } else {
            accept(LPAREN);
            Node node = expr();
            accept(RPAREN);
            return node;
        }
    }

    private Node term() {
        Node node = factor();

        while (this.token.getType() == MULTIPLY || this.token.getType() == DIVIDE || this.token.getType() == MODULO) {
            if (this.token.getType() == MULTIPLY) {
                accept(MULTIPLY);
                node = new Multiply(node, factor());
            } else if (this.token.getType() == MODULO) {
                accept(MODULO);
                node = new Modulo(node, factor());
            } else {
                accept(DIVIDE);
                node = new Divide(node, factor());
            }
        }

        return node;
    }

    private Node expr() {
        Node node = term();

        while (this.token.getType() == PLUS || this.token.getType() == MINUS) {
            if (this.token.getType() == PLUS) {
                accept(PLUS);
                node = new Plus(node, term());
            } else {
                accept(MINUS);
                node = new Minus(node, term());
            }
        }

        return node;
    }
}
