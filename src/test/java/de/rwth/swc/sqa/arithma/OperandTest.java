package de.rwth.swc.sqa.arithma;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OperandTest {

    @Test
    void evaluate() {
        Operand operand = new Operand(10);
        assertEquals(10, operand.evaluate());
    }

}