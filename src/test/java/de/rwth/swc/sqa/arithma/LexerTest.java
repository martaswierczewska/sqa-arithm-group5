package de.rwth.swc.sqa.arithma;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LexerTest {

    @Test
    void getNextTokenWithEmptyInput() {
        Lexer lexer = new Lexer("   ");
        Token result = lexer.getNextToken();
        assertEquals(Token.Type.EOF, result.getType());
        assertEquals("", result.getValue());
    }

    @Test
    void getNextTokenWithExpressionInput() {
        Lexer lexer = new Lexer("56+34");
        Token number1 = lexer.getNextToken();
        assertEquals(Token.Type.INTEGER, number1.getType());
        assertEquals("56", number1.getValue());

        Token operator = lexer.getNextToken();
        assertEquals(Token.Type.PLUS, operator.getType());
        assertEquals("+", operator.getValue());

        Token number2 = lexer.getNextToken();
        assertEquals(Token.Type.INTEGER, number2.getType());
        assertEquals("34", number2.getValue());
    }

    @Test
    void getNextTokenWithBracketExpressionInput() {
        Lexer lexer = new Lexer("(24)");
        Token lBracket = lexer.getNextToken();
        assertEquals(Token.Type.LPAREN, lBracket.getType());
        assertNull(lBracket.getValue());

        Token number = lexer.getNextToken();
        assertEquals(Token.Type.INTEGER, number.getType());
        assertEquals("24", number.getValue());

        Token rBracket = lexer.getNextToken();
        assertEquals(Token.Type.RPAREN, rBracket.getType());
        assertNull(rBracket.getValue());
    }

}