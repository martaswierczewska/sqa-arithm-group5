package de.rwth.swc.sqa.arithma;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ParserTest {


    @Test
    void parseWithSingleNumber() {
        final String expression = "(18)";
        final Lexer lex = new Lexer(expression);
        final Parser p = new Parser(lex);
        final Node n = p.parse();

        assertEquals(18, n.evaluate());
    }

    @Test
    void parseWithSingleOperationWithoutBrackets() {
        final String expression = "8/4";
        final Lexer lex = new Lexer(expression);
        final Parser p = new Parser(lex);
        final Node n = p.parse();

        assertEquals(2, n.evaluate());
    }

    @Test
    void parseWithSingleOperationWithBrackets() {
        final String expression = "(8/4)";
        final Lexer lex = new Lexer(expression);
        final Parser p = new Parser(lex);
        final Node n = p.parse();

        assertEquals(2, n.evaluate());
    }

    @Test
    void parseWithSingleOperationWithBracketsForEachOperand() {
        final String expression = "(8)/(4)";
        final Lexer lex = new Lexer(expression);
        final Parser p = new Parser(lex);
        final Node n = p.parse();
        
        assertEquals(2, n.evaluate());
    }


    @Test
    void parseWithMultipleOperationsWithoutBrackets() {
        final String expression = "18+8/4";
        final Lexer lex = new Lexer(expression);
        final Parser p = new Parser(lex);
        final Node n = p.parse();

        assertEquals(20, n.evaluate());
    }

    @Test
    void parseWithMultipleOperationsWithBrackets() {
        final String expression = "(18+2)/(4)";
        final Lexer lex = new Lexer(expression);
        final Parser p = new Parser(lex);
        final Node n = p.parse();

        assertEquals(5, n.evaluate());
    }

    @Test
    void parseWithMultipleOperationsWithBracketsForEachOperation() {
        final String expression = "(18+8)/(4-1)";
        final Lexer lex = new Lexer(expression);
        final Parser p = new Parser(lex);
        final Node n = p.parse();
        
        assertEquals(8, n.evaluate());
    }

    @Test
    void parseWithOnlyOperations() {
        final String expression = "+-*";
        final Lexer lex = new Lexer(expression);
        final Parser p = new Parser(lex);
        final Node n = p.parse();

        assertEquals(0, n.evaluate());
    }

    @Test
    void parseWithSingleOperationNegativeValues() {
        final String expression = "(-3+3)";
        final Lexer lex = new Lexer(expression);
        final Parser p = new Parser(lex);
        final Node n = p.parse();

        assertEquals(0, n.evaluate());
    }

    @Test
    void parseWithMultipleOperationsNegativeValues() {
        final String expression = "(-3-3)-(-4-7)";
        final Lexer lex = new Lexer(expression);
        final Parser p = new Parser(lex);
        final Node n = p.parse();

        assertEquals(5, n.evaluate());
    }

    @Test
    void parseWithSingleNegativeNumber() {
        final String expression = "(-50)";
        final Lexer lex = new Lexer(expression);
        final Parser p = new Parser(lex);
        final Node n = p.parse();

        assertEquals(-50, n.evaluate());
    }
    
}