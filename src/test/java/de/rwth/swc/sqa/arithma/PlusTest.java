package de.rwth.swc.sqa.arithma;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class PlusTest {

    private static Stream<Arguments> provideDataForTesting() {
        return Stream.of(
                Arguments.of(1, 2, 3),
                Arguments.of(4141, 0, 4141),
                Arguments.of(0, -8, -8),
                Arguments.of(-16, 4, -12)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForTesting")
    void evaluate(int left, int right, int expected) {
        Node leftNode = new Operand(left);
        Node rightNode = new Operand(right);
        Plus plus = new Plus(leftNode, rightNode);
        assertEquals(expected, plus.evaluate());
    }
}