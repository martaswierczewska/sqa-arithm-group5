package de.rwth.swc.sqa.arithma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ModuloTest {

    private static Stream<Arguments> provideDataForTesting() {
        return Stream.of(
                Arguments.of(10, 10, 0),
                Arguments.of(0, 6, 0),
                Arguments.of(30, 25, 5),
                Arguments.of(-5, -8, -5),
                Arguments.of(-67, 4, -3)
        );
    }


    @ParameterizedTest
    @MethodSource("provideDataForTesting")
    void evaluate(int left, int right, int expected) {
        Node leftNode = new Operand(left);
        Node rightNode = new Operand(right);
        Modulo modulo = new Modulo(leftNode, rightNode);
        assertEquals(expected, modulo.evaluate());
    }

    @Test
    void moduloByZero() {
        Node leftNode = new Operand(3);
        Node rightNode = new Operand(0);
        Modulo modulo = new Modulo(leftNode, rightNode);
        Assertions.assertThrows(ArithmeticException.class, modulo::evaluate);
    }

}