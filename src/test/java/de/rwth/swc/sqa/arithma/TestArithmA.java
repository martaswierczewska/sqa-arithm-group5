package de.rwth.swc.sqa.arithma;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TestArithmA {

    @Test
    void demonstrationTest() {
        final String expression = "(20 + 18)";

        final Lexer lex = new Lexer(expression);
        final Parser p = new Parser(lex);
        final Node n = p.parse();

        assertEquals(38, n.evaluate());
    }


    @ParameterizedTest
    @MethodSource("de.rwth.swc.sqa.ExpressionsToTest#provideExpressionsForAddition")
    void shouldAdd(String expression, long result) {
        final Node n = parseExpression(expression);
        assertEquals(result, n.evaluate());
    }

    @ParameterizedTest
    @MethodSource("de.rwth.swc.sqa.ExpressionsToTest#provideExpressionsForSubtraction")
    void shouldSubtract(String expression, long result) {
        final Node n = parseExpression(expression);
        assertEquals(result, n.evaluate());
    }

    @ParameterizedTest
    @MethodSource("de.rwth.swc.sqa.ExpressionsToTest#provideExpressionsForMultiplication")
    void shouldMultiply(String expression, long result) {
        final Node n = parseExpression(expression);
        assertEquals(result, n.evaluate());
    }

    @ParameterizedTest
    @MethodSource("de.rwth.swc.sqa.ExpressionsToTest#provideExpressionsForDivision")
    void shouldDivide(String expression, long result) {
        final Node n = parseExpression(expression);
        assertEquals(result, n.evaluate());
    }

    @ParameterizedTest
    @MethodSource("de.rwth.swc.sqa.ExpressionsToTest#provideExpressionsForModulo")
    void shouldReturnModulo(String expression, long result) {
        final Node n = parseExpression(expression);
        assertEquals(result, n.evaluate());
    }

    @ParameterizedTest
    @MethodSource("de.rwth.swc.sqa.ExpressionsToTest#provideExpressionsForExceptions")
    void shouldThrowArithmeticException(String expression) {
        final Node n = parseExpression(expression);
        assertThrows(ArithmeticException.class, n::evaluate);
    }

    @ParameterizedTest
    @MethodSource("de.rwth.swc.sqa.ExpressionsToTest#provideInvalidExpressions")
    void shouldThrowException(String expression) {
        final Node n = parseExpression(expression);
        assertThrows(Exception.class, n::evaluate);
    }

    private Node parseExpression(String expression) {
        final Lexer lex = new Lexer(expression);
        final Parser p = new Parser(lex);
        return p.parse();
    }
}
