package de.rwth.swc.sqa.arithma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class DivideTest {

    private static Stream<Arguments> provideDataForTesting() {
        return Stream.of(
                Arguments.of(1, 2, 0),
                Arguments.of(0, -8, 0),
                Arguments.of(-21, -21, 1),
                Arguments.of(-16, 4, -4),
                Arguments.of(3,4,0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForTesting")
    void evaluate(int left, int right, int expected) {
        Node leftNode = new Operand(left);
        Node rightNode = new Operand(right);
        Divide divide = new Divide(leftNode, rightNode);
        assertEquals(expected, divide.evaluate());
    }

    @Test
    void divideByZero() {
        Node leftNode = new Operand(120);
        Node rightNode = new Operand(0);
        Divide divide = new Divide(leftNode, rightNode);
        Assertions.assertThrows(ArithmeticException.class, divide::evaluate);
    }

}