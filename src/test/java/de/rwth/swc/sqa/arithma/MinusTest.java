package de.rwth.swc.sqa.arithma;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class MinusTest {

    private static Stream<Arguments> provideDataForTesting() {
        return Stream.of(
                Arguments.of(45, 25, 20),
                Arguments.of(30, 0, 30),
                Arguments.of(0, -20, 20),
                Arguments.of(-18, 4, -22)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForTesting")
    void evaluate(int left, int right, int expected) {
        Node leftNode = new Operand(left);
        Node rightNode = new Operand(right);
        Minus minus = new Minus(leftNode, rightNode);
        assertEquals(expected, minus.evaluate());
    }

}