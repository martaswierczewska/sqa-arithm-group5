package de.rwth.swc.sqa;

import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

public class ExpressionsToTest {

    public static Stream<Arguments> provideExpressionsForAddition() {
        return Stream.of(
                Arguments.of("5+10", 15),
                Arguments.of("5+0", 5),
                Arguments.of("(5+10)", 15),
                Arguments.of("(5)+(10)", 15),
                Arguments.of("0+0", 0),
                Arguments.of("-5+10", 5),
                Arguments.of("5+(-10)", -5),
                Arguments.of("-5+(-10)", -15),
                Arguments.of("(5+10)+(-15)", 0),
                Arguments.of("(5+10)+(15+5)+1+2+(2)", 40),
                Arguments.of("2147483647+5", 2147483652L),
                Arguments.of("-2147483647+(-5)", -2147483652L)
        );
    }

    public static Stream<Arguments> provideExpressionsForSubtraction() {
        return Stream.of(
                Arguments.of("5-10", -5),
                Arguments.of("5-0", 5),
                Arguments.of("(5-10)", -5),
                Arguments.of("(5)-(10)", -5),
                Arguments.of("0-0", 0),
                Arguments.of("-5-10", -15),
                Arguments.of("5-(-10)", 15),
                Arguments.of("-5-(-10)", 10),
                Arguments.of("(5-10)-(-15)", 0),
                Arguments.of("(5-10)-(15)-1-5-(4)", -30),
                Arguments.of("-2147483647-5", -2147483652L),
                Arguments.of("2147483647-(-5)", 2147483652L)
        );
    }

    public static Stream<Arguments> provideExpressionsForMultiplication() {
        return Stream.of(
                Arguments.of("5*10", 50),
                Arguments.of("5*0", 0),
                Arguments.of("(5*10)", 50),
                Arguments.of("(5)*(10)", 50),
                Arguments.of("0*0", 0),
                Arguments.of("-5*10", -50),
                Arguments.of("5*(-10)", -50),
                Arguments.of("-5*(-10)", 50),
                Arguments.of("(5-10)*(-15)", 225),
                Arguments.of("2*2*2*4*(10)", 320),
                Arguments.of("429496729*5", 2147483645L),
                Arguments.of("429496729*(-5)", -2147483645L)
        );
    }

    public static Stream<Arguments> provideExpressionsForDivision() {
        return Stream.of(
                Arguments.of("10/5", 2),
                Arguments.of("11/5", 2),
                Arguments.of("(5/10)", 0),
                Arguments.of("(5)/(10)", 0),
                Arguments.of("10/2/5", 1),
                Arguments.of("(10/2)/5", 1),
                Arguments.of("10/(-2)", -5),
                Arguments.of("-10/(2)", -5)
        );
    }

    public static Stream<Arguments> provideExpressionsForModulo() {
        return Stream.of(
                Arguments.of("10%5", 0),
                Arguments.of("20%20%2", 0),
                Arguments.of("20%(10%4)", 0),
                Arguments.of("10%3", 1),
                Arguments.of("-10%3", 2),
                Arguments.of("10%(-3)", 1)
        );
    }

    public static Stream<Arguments> provideExpressionsForExceptions() {
        return Stream.of(
                Arguments.of("10/0"),
                Arguments.of("20%0")
        );
    }

    public static Stream<Arguments> provideInvalidExpressions() {
        return Stream.of(
                Arguments.of("+5+5"),
                Arguments.of("+5+-+5"),
                Arguments.of("(((5))+5")
        );
    }
}
