package de.rwth.swc.sqa.arithmb;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SubtractionTest {

    private Subtraction subtraction;

    @BeforeAll
    public void initialize() {
        subtraction = new Subtraction();
    }

    private static Stream<Arguments> provideDataForTesting() {
        return Stream.of(
                Arguments.of(45, 25, 20),
                Arguments.of(30, 0, 30),
                Arguments.of(0, -20, 20),
                Arguments.of(-18, 4, -22)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForTesting")
    void evaluate(int left, int right, int expected) {
        assertEquals(expected, subtraction.evaluate(left, right));
    }

}