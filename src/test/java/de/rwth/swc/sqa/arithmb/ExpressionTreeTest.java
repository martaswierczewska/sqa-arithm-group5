package de.rwth.swc.sqa.arithmb;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExpressionTreeTest {

    @Test
    void calculateWithSingleOperation() {
        Node root = new Node();
        root.setOperator(new Addition());
        Node left = new Node();
        left.setValue(4);
        Node right = new Node();
        right.setValue(3);
        ExpressionTree expressionTree = new ExpressionTree(root);
        expressionTree.addLeft(root, right);
        expressionTree.addRight(root, left);
        int result = expressionTree.calculate();
        assertEquals(7, result);
    }

    @Test
    void calculateWithOnlyOperations() {
        Node root = new Node();
        root.setOperator(new Addition());

        Node divideOperation = new Node();
        divideOperation.setOperator(new Division());

        Node subtractOperation = new Node();
        subtractOperation.setOperator(new Subtraction());

        ExpressionTree expressionTree = new ExpressionTree(root);
        expressionTree.addLeft(root, divideOperation);
        expressionTree.addRight(root, subtractOperation);
        int result = expressionTree.calculate();
        assertEquals(0, result);
    }

    @Test
    void calculateWithOnlyRoot() {
        Node root = new Node();
        root.setValue(77);

        ExpressionTree expressionTree = new ExpressionTree(root);
        int result = expressionTree.calculate();
        assertEquals(77, result);
    }

    @Test
    void calculateWithMultipleOperations() {
        Node root = new Node();
        root.setOperator(new Addition());

        Node divideOperation = new Node();
        divideOperation.setOperator(new Division());

        Node divisionLeftOperand = new Node();
        divisionLeftOperand.setValue(18);

        Node divisionRightOperand = new Node();
        divisionRightOperand.setValue(6);

        Node subtractOperation = new Node();
        subtractOperation.setOperator(new Subtraction());

        Node subtractionLeftOperand = new Node();
        subtractionLeftOperand.setValue(3);

        Node subtractionRightOperand = new Node();
        subtractionRightOperand.setValue(2);

        ExpressionTree expressionTree = new ExpressionTree(root);
        expressionTree.addLeft(root, divideOperation);
        expressionTree.addRight(root, subtractOperation);
        expressionTree.addLeft(divideOperation, divisionLeftOperand);
        expressionTree.addRight(divideOperation, divisionRightOperand);
        expressionTree.addLeft(subtractOperation, subtractionLeftOperand);
        expressionTree.addRight(subtractOperation, subtractionRightOperand);
        int result = expressionTree.calculate();
        assertEquals(4, result);
    }

}