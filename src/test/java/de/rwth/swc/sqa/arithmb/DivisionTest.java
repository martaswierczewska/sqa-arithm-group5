package de.rwth.swc.sqa.arithmb;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DivisionTest {

    private Division division;

    @BeforeAll
    public void initialize() {
        division = new Division();
    }

    private static Stream<Arguments> provideDataForTesting() {
        return Stream.of(
                Arguments.of(1, 2, 0),
                Arguments.of(0, -8, 0),
                Arguments.of(-21, -21, 1),
                Arguments.of(-16, 4, -4),
                Arguments.of(3,4,0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForTesting")
    void evaluate(int left, int right, int expected) {
        assertEquals(expected, division.evaluate(left, right));
    }

    @Test
    void divideByZero() {
        Assertions.assertThrows(ArithmeticException.class, () -> division.evaluate(13,0));
    }

}