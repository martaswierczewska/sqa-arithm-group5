package de.rwth.swc.sqa.arithmb;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MathParserTest {


    @Test
    void evalWithSingleNumber() {
        final String expression = "(18)";
        final MathParser parser = new MathParser(expression);

        assertEquals(18, parser.eval());
    }

    @Test
    void evalWithSingleOperationWithoutBrackets() {
        final String expression = "8/4";
        final MathParser parser = new MathParser(expression);

        assertEquals(2, parser.eval());
    }

    @Test
    void evalWithSingleOperationWithBrackets() {
        final String expression = "(8/4)";
        final MathParser parser = new MathParser(expression);

        assertEquals(2, parser.eval());
    }

    @Test
    void evalWithSingleOperationWithBracketsForEachOperand() {
        final String expression = "(8)/(4)";
        final MathParser parser = new MathParser(expression);

        assertEquals(2, parser.eval());
    }


    @Test
    void evalWithMultipleOperationsWithoutBrackets() {
        final String expression = "18+8/4";
        final MathParser parser = new MathParser(expression);

        assertEquals(20, parser.eval());
    }

    @Test
    void evalWithMultipleOperationsWithBrackets() {
        final String expression = "(18+2)/(4)";
        final MathParser parser = new MathParser(expression);

        assertEquals(5, parser.eval());
    }

    @Test
    void evalWithMultipleOperationsWithBracketsForEachOperation() {
        final String expression = "(18+8)/(4-1)";
        final MathParser parser = new MathParser(expression);

        assertEquals(8, parser.eval());
    }

    @Test
    void evalWithOnlyOperations() {
        final String expression = "+-*";
        final MathParser parser = new MathParser(expression);

        assertEquals(0, parser.eval());
    }

    @Test
    void evalWithSingleOperationNegativeValues() {
        final String expression = "(-3+3)";
        final MathParser parser = new MathParser(expression);

        assertEquals(0, parser.eval());
    }

    @Test
    void evalWithMultipleOperationsNegativeValues() {
        final String expression = "(-3-3)-(-4-7)";
        final MathParser parser = new MathParser(expression);

        assertEquals(5, parser.eval());
    }

    @Test
    void evalWithSingleNegativeNumber() {
        final String expression = "(-50)";
        final MathParser parser = new MathParser(expression);

        assertEquals(-50, parser.eval());
    }


}