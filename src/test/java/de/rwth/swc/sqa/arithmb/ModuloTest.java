package de.rwth.swc.sqa.arithmb;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ModuloTest {

    private Modulo modulo;

    @BeforeAll
    public void initialize() {
        modulo = new Modulo();
    }

    private static Stream<Arguments> provideDataForTesting() {
        return Stream.of(
                Arguments.of(10, 10, 0),
                Arguments.of(0, 6, 0),
                Arguments.of(30, 25, 5),
                Arguments.of(-5, -8, -5),
                Arguments.of(-67, 4, -3)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForTesting")
    void evaluate(int left, int right, int expected) {
        assertEquals(expected, modulo.evaluate(left, right));
    }

    @Test
    void moduloByZero() {
        Assertions.assertThrows(ArithmeticException.class, () -> modulo.evaluate(54,0));
    }

}