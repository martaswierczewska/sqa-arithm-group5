package de.rwth.swc.sqa.arithmb;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MultiplicationTest {

    private Multiplication multiplication;

    @BeforeAll
    public void initialize() {
        multiplication = new Multiplication();
    }

    private static Stream<Arguments> provideDataForTesting() {
        return Stream.of(
                Arguments.of(43, 3, 129),
                Arguments.of(634, 0, 0),
                Arguments.of(2, -8, -16),
                Arguments.of(-5, -5, 25),
                Arguments.of(-16, 1, -16)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForTesting")
    void evaluate(int left, int right, int expected) {
        assertEquals(expected, multiplication.evaluate(left, right));
    }

}