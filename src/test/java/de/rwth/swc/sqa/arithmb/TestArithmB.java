package de.rwth.swc.sqa.arithmb;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TestArithmB {

    @Test
    void demonstrationTest() {
        final String expression = "(20 + 18)";
        final MathParser parser = new MathParser(expression);

        assertEquals(38, parser.eval());
    }

    @ParameterizedTest
    @MethodSource("de.rwth.swc.sqa.ExpressionsToTest#provideExpressionsForAddition")
    void shouldAdd(String expression, long result) {
        final MathParser parser = new MathParser(expression);
        assertEquals(result, parser.eval());
    }

    @ParameterizedTest
    @MethodSource("de.rwth.swc.sqa.ExpressionsToTest#provideExpressionsForSubtraction")
    void shouldSubtract(String expression, long result) {
        final MathParser parser = new MathParser(expression);
        assertEquals(result, parser.eval());
    }

    @ParameterizedTest
    @MethodSource("de.rwth.swc.sqa.ExpressionsToTest#provideExpressionsForDivision")
    void shouldDivide(String expression, long result) {
        final MathParser parser = new MathParser(expression);
        assertEquals(result, parser.eval());
    }

    @ParameterizedTest
    @MethodSource("de.rwth.swc.sqa.ExpressionsToTest#provideExpressionsForModulo")
    void shouldReturnModulo(String expression, long result) {
        final MathParser parser = new MathParser(expression);
        assertEquals(result, parser.eval());
    }

    @ParameterizedTest
    @MethodSource("de.rwth.swc.sqa.ExpressionsToTest#provideExpressionsForExceptions")
    void shouldThrowArithmeticException(String expression) {
        final MathParser parser = new MathParser(expression);
        assertThrows(ArithmeticException.class, parser::eval);
    }

    @ParameterizedTest
    @MethodSource("de.rwth.swc.sqa.ExpressionsToTest#provideInvalidExpressions")
    void shouldThrowException(String expression) {
        final MathParser parser = new MathParser(expression);
        assertThrows(Exception.class, parser::eval);
    }
}
